<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('package_id');
            $table->string('member_name');
            $table->string('member_fname');
            $table->string('member_phone',20);
            $table->string('member_cnic',25);
            $table->string('reference',50);
            $table->string('date');
            $table->string('reason_of_join');
            $table->string('medical_history',15);
            $table->string('address');
            $table->integer('discount');
            $table->integer('total');
            $table->string('member_image');
            $table->foreign('package_id')->references('id')->on('packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
