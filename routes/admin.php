<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "Admin" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\FeeController;
use App\Http\Controllers\Admin\MemberController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EnquiryController;
use Illuminate\Support\Facades\Route;

Route::get('/admin', function () {
    return redirect(route('admin.login'));
});
//---------------------------- AUTH ROUTES START --------------------------------//
Route::get('admin/login', [UserController::class, 'login'])->name('admin.login');
Route::post('loginstore', [UserController::class, 'loginstore'])->name('admin.loginstore');
Route::get('admin/logout', [UserController::class, 'logout'])->name('admin.logout');
//---------------------------- AUTH ROUTES END --------------------------------//


Route::group(['middleware' => 'auth'], function () {
    //---------------------------- MEMBERS-PACKAGE-TRAINERS ROUTES START --------------------------------//
    Route::group(['prefix' => 'admin/members'], function () {
        Route::get('/index', [MemberController::class, 'index'])->name('admin.member.index');
        Route::get('/create', [MemberController::class, 'create'])->name('admin.member.create');
        Route::get('/view/{id}', [MemberController::class, 'view'])->name('admin.member.view');
        Route::get('/edit/{id}', [MemberController::class, 'edit'])->name('admin.member.edit');
        Route::post('/store', [MemberController::class, 'memberStore'])->name('admin.member.store');
        Route::post('/update', [MemberController::class, 'update'])->name('admin.member.update');
        Route::post('/delete', [MemberController::class, 'delete'])->name('admin.member.delete');
        Route::post('/storepk', [MemberController::class, 'storePackage'])->name('admin.member.storePackage');
        Route::post('/viewpk', [MemberController::class, 'viewPackage'])->name('admin.member.viewPackage');
        Route::post('/storetr', [MemberController::class, 'storeTrainer'])->name('admin.member.storeTrainer');
        Route::post('/edittr', [MemberController::class, 'editTrainer'])->name('admin.member.editTrainer');
        Route::post('deletepk', [MemberController::class, 'deletePackage'])->name('admin.member.deletePackage');
        Route::post('deletetr', [MemberController::class, 'deleteTrainer'])->name('admin.member.deleteTrainer');
        Route::post('packgaetrainer', [MemberController::class, 'packgaetrainer'])->name('admin.member.packgaetrainer');
    });
    //---------------------------- MEMBERS-PACKAGE-TRAINERS ROUTES End --------------------------------//

    //---------------------------- Admin Dashboard ROUTES START --------------------------------//
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('admin.dashboard');
    });
    //---------------------------- Admin Dashboard ROUTES End --------------------------------//
    //---------------------------- FEE ROUTES START --------------------------------//
    Route::group(['prefix' => 'admin/fee'], function () {
        Route::get('/index', [FeeController::class, 'index'])->name('admin.fee.index');
        Route::get('/create', [FeeController::class, 'create'])->name('admin.fee.create');
        Route::get('/view/{id}', [FeeController::class, 'view'])->name('admin.fee.view');
        Route::post('/find_member', [FeeController::class, 'find_member'])->name('admin.fee.find_member');
        Route::post('/paynow', [FeeController::class, 'PayNow'])->name('admin.fee.paynow');
    });
    //---------------------------- FEE ROUTES END --------------------------------//

      //---------------------------- ENQUIRY ROUTES START --------------------------------//
      Route::group(['prefix' => 'admin/enquiry'], function () {
        Route::get('/index', [EnquiryController::class, 'index'])->name('admin.enquiry.index');
    });
    //---------------------------- ENQUIRY ROUTES END --------------------------------//

    //---------------------------- USER ROUTES START --------------------------------//
    Route::group(['prefix' => 'admin/user'], function () {
        Route::get('/index', [UserController::class, 'index'])->name('admin.user.index');
    });
    //---------------------------- USER ROUTES END --------------------------------//
});
