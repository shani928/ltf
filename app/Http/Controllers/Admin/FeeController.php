<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MemberFee;
use App\Models\Member;
use App\Models\Package;

class FeeController extends Controller
{
    public function index()
    {
        $memberfee = MemberFee::all();
        return view('Admin.Fee.index',compact('memberfee'));
    }

    public function create()
    {
        return view('Admin.Fee.create');
    }
    public function  find_member(Request $request)
    {
        $Member = Member::where('id','=',$request->member_id)->first();
         return [
            'id' => $Member->id,
             'member_name' => $Member->member_name,
             'member_phone' => $Member->member_phone,
             'package' => $Member->package->package_name,
             'package_amount' => $Member->package->package_amount,
             'balance' =>  $Member->balance,
             'paid' =>   $Member->balance,
             'trainer_fee' =>  $Member->trainer->trainer_fee ?? 0,
             'Registeration_fee' =>  $Member->member_regfee_status == 'unpaid' ?  $Member->package->reg_fees : 'Already Paid',
         ];
    }

    public function PayNow(Request $request){
        
             
        $member= Member::where('id','=',$request->member_id)->first();

        $paynow= new MemberFee;
        $paynow->member_id= $request->member_id;
        $paynow->trainer_fee= $request->trainer_fee;
        $paynow->registeration_fee= $request->registeration_fee == 'Already Paid' ?? 0;
        $paynow->package_amount= $request->package_amount;
        $paynow->fee_amount= $request->pay_amount;
        $paynow->total_amount= $request->member_total;
        $paynow->member_balance= $request->member_balance;
        $paynow->month_of= $request->month_of;
        $paynow->save();
        $expire = $member->pk_expiry;
        $package_start = $member->pk_start;
        if($request->renew_package == 1){
            $expire = 0;
        $package = Package::where('id', '=', $member->package_id)->first();
         $expire = date('Y-m-d', strtotime('+'.$package->package_duration.' month', strtotime($member->pk_expiry)));
        $package_start = $member->pk_expiry;
           
        }

        $member= Member::where('id','=',$request->member_id)->update([
            'balance' =>$request->member_balance,
            'member_regfee_status' => 'paid',
            'pk_start' => $package_start,
            'pk_expiry' => $expire
        ]);

        return redirect()->back();

    }

    public function view($id)
    {
        $invoice= MemberFee::find($id);
        return view('Admin.Fee.view',compact('invoice'));
    }
 
}
