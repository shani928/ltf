<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberFee;
use App\Models\Package;
use App\Models\Trainer;
use Carbon\Carbon;

class MemberController extends Controller
{

    //------------- MEMBER FUNCTIONS -------------------//
    public function index()
    {
        $member = Member::all();
        return view('Admin.Members.index', compact('member'));
    }

    public function create()
    {
        $trainer =  Trainer::all();
        $package = Package::all();
        $active_trainer = Trainer::all();
        $active_package = Package::where('status', '=', '1')->get();
        return view('Admin.Members.members', compact('package', 'trainer', 'active_package', 'active_trainer'));
    }

    public function memberStore(Request $request)
    {
        $this->validate($request, [
            'member_name' => 'required',
            'member_fname' => 'required',
            'member_name' => 'required',
            'member_phone' => 'required',
            'member_reference' => 'required',
            'member_dob' => 'required',
            'member_roj' => 'required',
            'member_medical' => 'required',
            'member_timings' => 'required',
            'member_adress' => 'required',
            'member_package' => 'required',
            'pk_start' => 'required',
        ]);

        $member = new Member;
        $member->package_id = $request->member_package;
        $member->member_name = $request->member_name;
        $member->member_fname = $request->member_fname;
        $member->member_phone = $request->member_phone;
        $member->member_cnic = $request->member_cnic;
        $member->reference = $request->member_reference;
        $member->date = $request->member_dob;
        $member->reason_of_join = $request->member_roj;
        $member->medical_history = $request->member_medical;
        $member->member_timings = $request->member_timings;
        $member->member_gender = $request->gender;
        $member->address = $request->member_adress;
        $member->trainer_id = $request->member_trainer;
        $member->member_regfee = $request->member_regfee == true ? 1 : 0;
        $member->member_regfee_status =$request->member_regfee == true ? 'unpaid' : 'paid';
        $member->balance = $request->member_totalfee;
        $member->total = $request->member_totalfee;
        $member->pk_start = $request->pk_start;
        $package = Package::where('id', '=', $request->member_package)->first();
         $expire = date('Y-m-d', strtotime('+'.$package->package_duration.' month', strtotime($request->pk_start)));
         $member->pk_expiry = $expire;
        if ($request->hasFile('member_image')) {

            $file = $request->file('member_image');
            $filename =  time() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/members/', $filename);
            $member->member_image = $filename;
        }
        $member->save();
        return redirect()->back()->with('success', 'Member Created Successfully!');
    }

    public function edit($id)
    {
        $member = Member::find($id);
        $active_trainer = Trainer::all();
        $active_package = Package::where('status', '=', '1')->get();
        return view('Admin.Members.edit', compact('active_package', 'active_trainer', 'member'));
    }
    public function update(Request $request)
    {
        $member = Member::find($request->member_id);
        $member->package_id = $request->member_package;
        $member->member_name = $request->member_name;
        $member->member_fname = $request->member_fname;
        $member->member_phone = $request->member_phone;
        $member->member_cnic = $request->member_cnic;
        $member->reference = $request->member_reference;
        $member->date = $request->member_dob;
        $member->reason_of_join = $request->member_roj;
        $member->medical_history = $request->member_medical;
        $member->member_timings = $request->member_timings;
        $member->member_gender = $request->gender;
        $member->member_status = $request->member_status;
        $member->freeze_date = $request->freez_date;
        $member->address = $request->member_adress;
        $member->trainer_id = $request->member_trainer;
        $member->member_regfee = $request->member_regfee == true ? 1 : 0;
        $member->total = $request->member_totalfee;
        if ($request->hasFile('member_image')) {
            $file = $request->file('member_image');
            $filename =  time() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/members/', $filename);
            $member->member_image = $filename;
        }
        $member->save();
        return redirect()->back()->with('success', 'Member Updated Successfully!');
    }

    public function delete(Request $request)
    {
        $member= Member::where('id','=',$request->mem_id)->delete();
        return response()->json(['status' => 'Member Deleted Successfully!']);
    }
    public function view($id){
        $member=Member::find($id);
        $mtrans=MemberFee::where('member_id','=',$id)->orderBy('id','DESC')->get();
        return view('Admin.Members.view',compact('member','mtrans'));
    }

    //------------- MEMBER FUNCTIONS END -------------------//
    public function storePackage(Request $request)
    {
        $pkcount = Package::where('id', '=', $request->data['pkid'])
            ->count();

        if ($pkcount == 0) {
            $package = new Package();
            $package->package_name =  $request->data['pkname'];
            $package->package_duration =  $request->data['pkduration'];
            $package->package_amount =  $request->data['pkamount'];
            $package->status =  $request->data['pkstatus'];
            $package->reg_fees = $request->data['reg_fees'];
            $package->save();
            return response()->json(['status' => 'Package Created Successfully!']);
        } else {

            $package = Package::where('id', '=', $request->data['pkid'])->update([
                'package_name' => $request->data['pkname'],
                'package_duration' => $request->data['pkduration'],
                'package_amount' => $request->data['pkamount'],
                'status' => $request->data['pkstatus'],
                'reg_fees' => $request->data['reg_fees'],
            ]);
            return response()->json(['status' => 'Package Updated Successfully!']);
        }
    }
    public function viewPackage(Request $request)
    {
        $packge = Package::where('id', '=', $request->data['id'])->get();
        return $packge;
    }

    public function storeTrainer(Request $request)
    {
        $trcount = Trainer::where('id', '=', $request->trid)
            ->count();
        if ($trcount == 0) {
            $trainer = new Trainer;
            $trainer->trainer_name = $request->trname;
            $trainer->gender = $request->trgender;
            $trainer->dob = $request->trdob;
            $trainer->trainer_fee = $request->trfee;
            $trainer->expertise = $request->trexpert;

            if ($request->hasFile('trimage')) {
                $file = $request->file('trimage');
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('storage/trainers/', $filename);
                $trainer->trainer_image = $filename;
            }
            $trainer->save();
            return response()->json(['status' => 'Trainer Created Successfully!']);
        } else {

            $trainer = Trainer::find($request->trid);
            $trainer->trainer_name = $request->trname;
            $trainer->gender = $request->trgender;
            $trainer->dob = $request->trdob;
            $trainer->trainer_fee = $request->trfee;
            $trainer->expertise = $request->trexpert;

            if ($request->hasFile('trimage')) {
                $file = $request->file('trimage');
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('storage/trainers/', $filename);
                $trainer->trainer_image = $filename;
            }
            $trainer->save();
            return response()->json(['status' => 'Trainer Updated Successfully!']);
        }
    }
    public function editTrainer(Request $request)
    {
        $trainer = Trainer::where('id', '=', $request->data['id'])->get();
        return $trainer;
    }

    public function deletePackage(Request $request)
    {
        $package = Package::where('id', '=', $request->package_id)->delete();
        return response()->json(['status' => 'Package Deleted Successfully!']);
    }

    public function deleteTrainer(Request $request)
    {
        $trainer = Trainer::where('id', '=', $request->trainer_id)->delete();
        return response()->json(['status' => 'Trainer Deleted Successfully!']);
    }
    public function packgaetrainer(Request $request)
    {
        $data = [];
        $t = 0;
        if($request->trainer_id != 0){
            $trainer = Trainer::where('id', '=', $request->trainer_id)->first();
            $t = $trainer->trainer_fee;
        }
        
        
        $package = Package::where('id', '=', $request->package_id)->first();
        $sum = $t + $package->package_amount;
        return $data[] = [
            "total" => $sum,
            "reg_fees" => $package->reg_fees,
        ];
    }



   


}
