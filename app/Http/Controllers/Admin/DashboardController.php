<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberFee;
use App\Models\Trainer;
use DB;
class DashboardController extends Controller
{
    public function dashboard(){
        $dashboard=[];
        $all_members = Member::all()->count();
        $all_active_members = Member::where('member_status',1)->count();
        $all_freez_members = Member::where('member_status',2)->count();
        $trainer= Trainer::all()->count();
        $female_members = Member::where('member_gender','Female')->where('member_status','!=',3)->count();
        $male_members = Member::where('member_gender','Male')->where('member_status','!=',3)->count();
         $today_amount = DB::select('select sum(fee_amount) as Today from member_fees where date_format(created_at, "%m-%y%d") = date_format(now(), "%m-%y%d") ');
         $monthly_amount = DB::select('select sum(fee_amount) as Monthly from member_fees where date_format(created_at, "%m-%y-%d") between date_format(now(), "%m-%y-01") AND date_format(now(), "%m-%y-%d")');
        $fiftyday_amount = DB::select('select sum(fee_amount) as "FiftenDays" from member_fees where date_format(created_at, "%m-%y-%d") between date_format( DATE_SUB(NOW(), INTERVAL 15 DAY), "%m-%y-%d") AND date_format(now(), "%m-%y-%d")');
         $balance = Member::where('member_status','!=',3)->sum('balance');
        $members_packages = DB::select('SELECT count(m.id) as total ,p.package_name  FROM `members` as m left join packages as p on p.id = m.package_id   group by p.package_name');
        $today_expire = DB::select('select count(id) as Today from members where date_format(pk_expiry, "%m-%y%d") = date_format(now(), "%m-%y%d") AND member_status != 3');
        $monthly_expire = DB::select('select count(id) as Monthly from members where date_format(pk_expiry, "%m-%y-%d") between date_format(now(), "%m-%y-01") AND date_format(now(), "%m-%y-%d") AND member_status != 3');
        $fiftyday_expire = DB::select('select count(id) as "FiftenDays" from members where date_format(pk_expiry, "%m-%y-%d") between date_format( DATE_SUB(NOW(), INTERVAL 15 DAY), "%m-%y-%d") AND date_format(now(), "%m-%y-%d") AND member_status != 3');
        
        $balance_details = Member::where('member_status','!=',3)->where('balance','>',0)->get();
        $a_member= Member::where('member_status','=',1)->get();
        $f_member=Member::where('member_status','=',2) ->get();
        $all_trainer= Trainer::all();   

        $today_amount_details = DB::select('select *  from member_fees as fee left join members as m on m.id = fee.member_id where date_format(fee.created_at, "%m-%y%d") = date_format(now(), "%m-%y%d") ');
        $monthly_amount_details = DB::select('select *  from member_fees as fee left join members as m on m.id = fee.member_id where date_format(fee.created_at, "%m-%y-%d") between date_format(now(), "%m-%y-01") AND date_format(now(), "%m-%y-%d")');
        $fiftyday_amount_details = DB::select('select *  from member_fees as fee left join members as m on m.id = fee.member_id where date_format(fee.created_at, "%m-%y-%d") between date_format( DATE_SUB(NOW(), INTERVAL 15 DAY), "%m-%y-%d") AND date_format(now(), "%m-%y-%d")');
        

        $today_expire_details = DB::select('select * from members where date_format(pk_expiry, "%m-%y%d") = date_format(now(), "%m-%y%d") AND member_status != 3');
        $monthly_expire_details = DB::select('select * from members where date_format(pk_expiry, "%m-%y-%d") between date_format(now(), "%m-%y-01") AND date_format(now(), "%m-%y-%d") AND member_status != 3');
        $fiftyday_expire_details = DB::select('select * from members where date_format(pk_expiry, "%m-%y-%d") between date_format( DATE_SUB(NOW(), INTERVAL 15 DAY), "%m-%y-%d") AND date_format(now(), "%m-%y-%d") AND member_status != 3');
        $dashboard[]= [
            "All_members" => $all_members,
            "All_active_members" => $all_active_members,
            "All_freez_members" => $all_freez_members,
            "members_packages" => $members_packages,
            "male_members" => $male_members,
            "female_members" => $female_members,
            "today_amount" => $today_amount,
            "monthly_amount" => $monthly_amount,
            "fiftyday_amount" => $fiftyday_amount,
            "balance" => $balance,
            "balance_details" => $balance_details,
            "members_packages" => $members_packages,
            "today_expire" => $today_expire,
            "monthly_expire" => $monthly_expire,
            "fiftyday_expire" => $fiftyday_expire,
            "trainer" => $trainer,
            "a_member" => $a_member,
            "f_member" => $f_member,
            "all_trainer" => $all_trainer,
            "today_amount_details" => $today_amount_details,
            "monthly_amount_details" => $monthly_amount_details,
            "fiftyday_amount_details" => $fiftyday_amount_details,
            "today_expire_details" => $today_expire_details,
            "monthly_expire_details" => $monthly_expire_details,
            "fiftyday_expire_details" => $fiftyday_expire_details
        ];
      
             
         return view('Admin.Dashboard.index',compact('dashboard'));
    }
}