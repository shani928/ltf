<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
            return view('Admin.User.index');
    }



    //------------------ AUTH FUNCTION----------------//
    public function login()
    {
        return view('Admin.Auth.login');
    }
    public function loginstore(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!Auth::attempt($request->only('email', 'password'))) {
            return redirect()->back()->with('error', 'Invalid Login Credentials');
        } else {
            return redirect()->route('admin.member.index');
        }
    }
    public function logout()
    {
        $user = Auth::logout();
        return redirect()->route('admin.login');
    }
    //------------------ AUTH FUNCTION END----------------//
}
