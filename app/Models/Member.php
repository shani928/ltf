<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'package_id','trainer_id', 'member_name', 'member_fname', 'member_phone', 'member_cnic', 'reference', 'date',
        'reason_of_join', 'medical_history', 'address', 'discount','member_regfee','member_regfee_status', 'total','balance', 'member_image'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class,'package_id','id');
    }

    public function trainer()
    {
        return $this->belongsTo(Trainer::class,'trainer_id','id');
    }
    public function memberfee()
    {
        return $this->hasMany(MemberFee::class);
    }
}
