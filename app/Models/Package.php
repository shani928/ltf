<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'package_name', 'package_duration', 'package_amount', 'status','reg_fees'
    ];

    public function member()
    {
        return $this->hasMany(Member::class);
    }
}
