<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberFee extends Model
{
    protected $fillable=[
        'member_id','trainer_fee','registeration_fee','month_of','fee_amount','member_balance'
    ];

    public function member()
    {
        return $this->belongsTo(Member::class,'member_id','id');
    }
}
