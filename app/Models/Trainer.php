<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $fillable=[
        'trainer_name','gender','dob','trainer_fee','expertise','trainer_image'
    ];

    public function member()
    {
        return $this->hasMany(Member::class);
    }
}
