<div class="sidebar-wrapper" data-simplebar="true">
			<div class="sidebar-header">
				<div class="">
					<img src="{{asset('fitnessback/assets/images/icons/lifetime.png')}}" class="logo-icon-2" alt="" />
				</div>
				<a href="javascript:;" class="toggle-btn ml-auto"> <i class="bx bx-menu"></i>
				</a>
			</div>
			<!--navigation-->
			<ul class="metismenu" id="menu">
				<li>
				<a href="{{route('admin.dashboard')}}">
						<div class="parent-icon"><i class="bx bx-home-alt"></i>
						</div>
						<div class="menu-title">Dashboard</div>
					</a>
				</li>

				<li>
					<a href="{{route('admin.member.index')}}">
						<div class="parent-icon"><i class="lni lni-users"></i>
						</div>
						<div class="menu-title">Member</div>
					</a>
				</li>
				<li>
					<a href="{{route('admin.fee.index')}}">
						<div class="parent-icon"><i class="lni lni-money-protection"></i>
						</div>
						<div class="menu-title">Fee Collection</div>
					</a>
				</li>
				<li>
					<a href="{{route('admin.user.index')}}">
						<div class="parent-icon"><i class="fadeIn animated bx bx-user"></i>
						</div>
						<div class="menu-title">User</div>
					</a>
				</li>
                <li>
					<a href="{{route('admin.enquiry.index')}}">
						<div class="parent-icon"><i class="fadeIn animated bx bx-user"></i>
						</div>
						<div class="menu-title">Inquiry</div>
					</a>
				</li>
			</ul>

			<!--end navigation-->
		</div>
