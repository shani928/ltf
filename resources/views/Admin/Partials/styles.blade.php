	<!-- Required meta tags -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<title>LIFETIME FITNESS</title>
	<!--favicon-->
	<link rel="icon" href="{{asset('fitnessback/assets/images/icons/lifetime.png')}}" type="image/png" />
	<!-- Vector CSS -->
	<link href="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
	<!--plugins-->
	<link href="{{asset('fitnessback/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
	<link href="{{asset('fitnessback/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
	<link href="{{asset('fitnessback/assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
	<!-- loader-->
	<link href="{{asset('fitnessback/assets/css/pace.min.css')}}" rel="stylesheet" />
	<script src="{{asset('fitnessback/assets/js/pace.min.js')}}"></script>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{asset('fitnessback/assets/css/bootstrap.min.css')}}" />
	<link href="{{asset('fitnessback/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
	<!-- Icons CSS -->
	<link rel="stylesheet" href="{{asset('fitnessback/assets/css/icons.css')}}" />
	<!-- App CSS -->
	<link rel="stylesheet" href="{{asset('fitnessback/assets/css/toastr.min.css')}}">
	<link rel="stylesheet" href="{{asset('fitnessback/assets/css/app.css')}}" />