<script src="{{asset('fitnessback/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/js/popper.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/js/bootstrap.min.js')}}"></script>
<!--plugins-->
<script src="{{asset('fitnessback/assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/metismenu/js/metisMenu.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
<!-- Vector map JavaScript -->
<script src="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-in-mill.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-uk-mill-en.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/vectormap/jquery-jvectormap-au-mill.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/apexcharts-bundle/js/apexcharts.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/js/index.js')}}"></script>
<!-- App JS -->
<script src="{{asset('fitnessback/assets/js/app.js')}}"></script>
<script src="{{asset('fitnessback/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('fitnessback/assets/js/toastr.min.js')}}"></script>
{{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> --}}
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
