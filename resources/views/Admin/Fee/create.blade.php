@extends('Admin.Layouts.master')
@section('content')

<style>
    .modal-dialog {
        margin-top: 100px;
    }

    .card {
        margin-bottom: 8px;
    }

    .fade:not(.show) {
        opacity: 1;
    }

    #create-package {
        display: block;
    }
</style>


<!--breadcrumb-->
<div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">
    <div class="breadcrumb-title pr-3">Fee</div>
    <div class="pl-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-user"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Add Fee</li>
            </ol>
        </nav>
    </div>
</div>
<!--end breadcrumb-->
<div class="card radius-15">
    <div class="card-body">
        <div class="card-title">
            <h4 class="mb-0">Add Fee</h4>
        </div>
        <hr />

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Member ID</label>
                    <input class="form-control" type="text" placeholder="Member Id" name="member_name" id="member_id">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label style="visibility: hidden;">Find sdfsdfsd sdfsd</label>
                    <input class="btn btn-danger" type="button" onclick="find_member()" value="Find Member">
                </div>
            </div>


        </div>
        <table class="table table-borderd">
            <tr>
                <th>Member ID</th>
                <th>Member Name</th>
                <th>Member Package</th>
                <th>Member Phone</th>
                <th>Balance</th>
</tr >
         
            <tr>
                <td id="id"></td>
                <td id="name"></td>
                <td id="package"></td>
                <td id="phone"></td>
                <td id="balance"></td>
            </tr>

        </table>

        <form action="{{route('admin.fee.paynow')}}" method="POST" id="paynow">
            
            @csrf
            <input type="hidden"  name="member_id" id="payid">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label> Trainer fee </label>
                        <input class="form-control" readonly type="text" id="trainer_fee" placeholder="Trainer fee" name="trainer_fee">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Registeration fee</label>
                        <input class="form-control" readonly type="text" id="Registeration_fee" placeholder="Registeration fee" name="registeration_fee">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Package Amount</label>
                        <input class="form-control" readonly type="text" id="package_amount" placeholder="Package Amount" name="package_amount">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label> Pay Amount </label>
                        <input class="form-control" id="pay_price" type="text" placeholder="Pay Amount" onkeyup="PayAmount()" name="pay_amount">

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Total </label>
                        <input class="form-control" readonly id="total" type="text" placeholder="Total" name="member_total">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Balance</label>
                        <input class="form-control" id="hidden_bal" type="hidden" placeholder="Member Phone">
                        <input  class="form-control" readonly id="bal" type="text" placeholder="Member Phone" name="member_balance">
                    </div>
                </div>
            </div>
<div class="row">
            <div class="col-md-4">
                    <div class="form-group">
                        <label>Month Of</label>
                        <input class="form-control"  type="date"   name="month_of">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label></label>
                        <div class="form-check">
										<input class="form-check-input is-invalid" name="renew_package" type="checkbox" value="1" id="invalidCheck3">
										<label class="form-check-label" for="invalidCheck3">Renew package</label>
										 
									</div>
                    </div>
                </div>
</div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" id="pay"  class="btn btn-light float-right"> <label><i class="fadeIn animated bx bx-money" style="margin-top: -24px;margin-right: 7px;"></i></label>
                            Pay Now</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
<script type="text/javascript">
document.querySelector('#paynow').addEventListener('submit', function(e) {
   
  var form = this;

  e.preventDefault(); // <--- prevent form from submitting

  swal({
      title: "Are you sure?",
      text: "You Want Collect This Member Fee?",
      icon: "warning",
      buttons: [
        'No, cancel it!',
        'Yes, I am sure!'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        swal({
          title: 'Collected!',
          text: 'Fee Collected Successfully!',
          icon: 'success'
        }).then(function() {
           $('#pay').prop('disabled', true);
          form.submit(); // <--- submit form programmatically
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    })
});
  
    // window.onbeforeunload = function() {
    //     return "Dude, are you sure you want to leave? Think of the kittens!";
    // }

</script>
<script>
    var bar = 0;

    function find_member() {
        var member_id = document.getElementById('member_id');
        var id = document.getElementById('id');
        var name = document.getElementById('name');
        var phone = document.getElementById('phone');
        var trainer_fee = document.getElementById('trainer_fee');
        var Registeration_fee = document.getElementById('Registeration_fee');
        var package_amount = document.getElementById('package_amount');
        var total = document.getElementById('total');
        var bal = document.getElementById('bal');
        var payid = document.getElementById('payid');
        var hidden_bal = document.getElementById('hidden_bal');
        $.ajax({
            url: '{{route("admin.fee.find_member")}}',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "member_id": member_id.value
            },
            success: function(res) {
                payid.value= res.id,
                id.innerHTML = res.id
                name.innerHTML = res.member_name
                phone.innerHTML = res.member_phone
                package.innerHTML = res.package
                balance.innerHTML = res.balance
                bar = res.balance
                hidden_bal.value = res.balance
                bal.value = res.balance
                trainer_fee.value = res.trainer_fee
                Registeration_fee.value = res.Registeration_fee
                package_amount.value = res.package_amount;
                if(res.Registeration_fee == 'Already Paid'){
                    res.Registeration_fee = 0;
                }
                total.value = (parseInt(res.trainer_fee) + parseInt(res.Registeration_fee) + parseInt(res.package_amount))
            }
        })

    }

    function PayAmount() {
        var bal = document.getElementById('bal');
        if(bal.value != 0){
        var pay_price = document.getElementById('pay_price');
       
        var hidden_bal = document.getElementById('hidden_bal');
        if (pay_price.value == '' || pay_price.value == 0 || pay_price.value == undefined) {
            bal.value = hidden_bal.value;
        }
         else {
            bal.value = (parseInt(hidden_bal.value) - parseInt(pay_price.value));
        }
        }
    }
</script>
@include('Admin.Partials.scripts')
@endsection