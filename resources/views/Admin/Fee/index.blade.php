@extends('Admin.Layouts.master')
@section('content')
<div class="card radius-15">
    <div class="card-body">
        <div class="card-title">
        <div class="row">
                <div class="col-md-10">
                    <h4 class="mb-0">View Transactions</h4>
                </div>
                <div class="col-md-2">
         <a class="btn btn-light float-right" href="{{route('admin.fee.create')}}"><i class="fadeIn animated bx bx-money" style="margin-top: -24px;margin-right: 7px;"></i>Collect Fee</a>
                </div>
            </div>
        </div>
        <hr />

        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Member Id</th>
                        <th> Member Name</th>
                        <th>Month Of </th>
                        <th>Total Amount</th>
                        <th>Paid Amount</th>
                        <th>Balance</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($memberfee as $fees)
                    <tr>
                        <td>{{$fees->member->id}}</td>
                        <td>{{$fees->member->member_name}}</td>
                        <td>{{ Carbon\Carbon::parse($fees->month_of)->format('D, jS \ F ') }}</td>
                        <td>{{$fees->member->total}}</td>
                        <td>{{$fees->fee_amount}}</td>
                        <td>{{number_format($fees->member_balance)}}</td>
                        <td> <a href="{{route('admin.fee.view',$fees->id)}}" style="padding: 4px;" class="btn btn-success"><i class="lni lni-eye"></i></td>
                    </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                    <th>Member Id</th>
                        <th> Member Name</th>
                        <th>Month Of </th>
                        <th>Paid Amount</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@include('Admin.Partials.scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection