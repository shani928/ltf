@extends('Admin.Layouts.master')
@section('content')
<base href="https://demos.telerik.com/kendo-ui/pdf-export/index">
    <style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
    <title></title>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.mobile.min.css" />

    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/jszip.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
<style>
.border-bottom {
    border-bottom: 1px solid #000 !important
}
.email a{
    color:#000
}

</style>

<div class="card">
    <div class="card-body">
        <div id="invoice">
            <div class="toolbar hidden-print">
                <div class="text-right">
                <a href="https://api.whatsapp.com/send?text={{$invoice->member->member_phone}}"><i class="fab fa-whatsapp"></i>fddd</a>
                    <button type="button" class="btn btn-light"><i class="fa fa-print"></i> Print</button>
                    <button type="button" class="btn btn-light export-pdf k-button"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
                </div>
                <hr>
            </div>

            <div id="example">

 <div class="demo-section k-content export-app wide hidden-on-narrow">
     <div class="demo-section content-wrapper wide">
       <!-- <div class="demo-section charts-wrapper wide"> -->
            <div class="invoice overflow-auto">
                <div style="min-width: 600px">

                    <main style="border:1px solid #ccc;padding:20px;">
                        <div class="row contacts">
                            <div class="col invoice-to">
                                <div class="text-gray-light">Cell : 0304331002 </div>


                                <div class="email"><a>Web www.lifetimefitness.pk</a>
                                </div>
                            </div>
                            <div class="col invoice-details">

                                <div class="date"><img src="{{asset('fitnessback/assets/images/icons/lifetime.png')}}"
                                        width="60%" alt="" /> </div>
                            </div>

                            <div class="col invoice-details" class="text-right">
                                <h3 class="invoice-id text-right">Payment is Refundable </h3>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3 ">
                                <div class="border-bottom text-gray-light">S.No : {{$invoice->id}} </div>
                            </div>
                            <div class="col-md-7">
                                <div class="text-gray-light  text-right">Date:</div>
                            </div>
                            <div class="col-md-2">
                                <div class="border-bottom text-gray-light text-center"> {{ Carbon\Carbon::parse($invoice->created_at)->format('D | jS \ F, Y') }} </div>
                            </div>
                        </div>


                        <!-- <div class="row mt-2">
                            <div class="col-md-9 overflow-hidden">
                                <div class=" text-gray-light">Membership No
                                    _________________________________________________________________________________________________________________________________
                                </div>
                            </div>

                            <div class="col-md-3 overflow-hidden">
                                <div class="text-gray-light text-center"> Starting Date ___________________________
                                </div>
                            </div>
                        </div> -->
 


                        <div class="row mt-2">
                            <div class="col-md-2 overflow-hidden">
                                <div class=" text-gray-light">Membership No </div>
                            </div>
                            <div class="col-md-5 overflow-hidden">
                                <div class="border-bottom text-gray-light  "> {{$invoice->member->id}} </div>
                            </div>
                            <div class="col-md-2 overflow-hidden">
                                <div class=" text-gray-light text-right">Starting Date </div>
                            </div>
                            <div class="col-md-3 overflow-hidden">
                                <div class="border-bottom text-gray-light  ">{{ Carbon\Carbon::parse($invoice->member->pk_start)->format('D | jS \ F, Y') }}</div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-1 overflow-hidden">
                                <div class=" text-gray-light">Name </div>
                            </div>
                            <div class="col-md-6 overflow-hidden">
                                <div class="border-bottom text-gray-light  "> {{ucwords($invoice->member->member_name)}} </div>
                            </div>

                            <div class="col-md-2 overflow-hidden">
                                <div class=" text-gray-light text-right">Contact No </div>
                            </div>
                            <div class="col-md-3 overflow-hidden">
                                <div class="border-bottom text-gray-light  "> {{$invoice->member->member_phone}}  </div>
                            </div>


                        </div>
                        <div class="row mt-2">
                            <div class="col-md-1 overflow-hidden">
                                <div class=" text-gray-light">Duration </div>
                            </div>
                            <div class="col-md-11 overflow-hidden">
                                <div class="border-bottom text-gray-light  "> {{$invoice->member->package['package_duration']}} Months </div>
                            </div>

                        </div>

                        <div class="row mt-2">
                            <div class="col-md-1 overflow-hidden">
                                <div class=" text-gray-light">Advance </div>
                            </div>
                            <div class="col-md-11 overflow-hidden">
                                <div class="border-bottom text-gray-light  ">{{$invoice->fee_amount}}</div>
                            </div>




                        </div>



                        <div class="row mt-2">
                            <div class="col-md-1 overflow-hidden">
                                <div class=" text-gray-light">Balance </div>
                            </div>
                            <div class="col-md-11 overflow-hidden">
                                <div class="border-bottom text-gray-light  ">{{$invoice->member_balance}}</div>
                            </div>




                        </div>


                        <!-- <table>
												<thead>
													<tr>
														<th>#</th>
														<th class="text-left">DESCRIPTION</th>
														<th class="text-right">HOUR PRICE</th>
														<th class="text-right">HOURS</th>
														<th class="text-right">TOTAL</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="no">04</td>
														<td class="text-left">
															<h3>
													<a target="_blank" href="javascript:;">
													Youtube channel
													</a>
													</h3>
															<a target="_blank" href="javascript:;">
													   Useful videos
												   </a> to improve your Javascript skills. Subscribe and stay tuned :)</td>
														<td class="unit">$0.00</td>
														<td class="qty">100</td>
														<td class="total">$0.00</td>
													</tr>
													<tr>
														<td class="no">01</td>
														<td class="text-left">
															<h3>Website Design</h3>Creating a recognizable design solution based on the company's existing visual identity</td>
														<td class="unit">$40.00</td>
														<td class="qty">30</td>
														<td class="total">$1,200.00</td>
													</tr>
													<tr>
														<td class="no">02</td>
														<td class="text-left">
															<h3>Website Development</h3>Developing a Content Management System-based Website</td>
														<td class="unit">$40.00</td>
														<td class="qty">80</td>
														<td class="total">$3,200.00</td>
													</tr>
													<tr>
														<td class="no">03</td>
														<td class="text-left">
															<h3>Search Engines Optimization</h3>Optimize the site for search engines (SEO)</td>
														<td class="unit">$40.00</td>
														<td class="qty">20</td>
														<td class="total">$800.00</td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td colspan="2"></td>
														<td colspan="2">SUBTOTAL</td>
														<td>$5,200.00</td>
													</tr>
													<tr>
														<td colspan="2"></td>
														<td colspan="2">TAX 25%</td>
														<td>$1,300.00</td>
													</tr>
													<tr>
														<td colspan="2"></td>
														<td colspan="2">GRAND TOTAL</td>
														<td>$6,500.00</td>
													</tr>
												</tfoot>
											</table> -->
                        <!-- <div class="thanks">Thank you!</div> -->
                        <div class="notices mt-3">
                            <div>Address:</div>
                            <div class="notice">Plot 64 A, Block, 2, Block 2 Shah Faisal Colony 2 Shah Faisal Colony. Karachi, Sindh, Pakistan-75230.
                            </div>
                        </div>
                    </main>
                    <footer>Invoice was created on a computer and is valid without the signature and seal.</footer>
                </div>
                <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                <div></div>
            </div>

            </div>
      
      </div>
  </div>
  
  <div class="responsive-message"></div>
 </div>
        </div>
    </div>
</div>

</div>




<style>
    /*
        Use the DejaVu Sans font for display and embedding in the PDF file.
        The standard PDF fonts have no support for Unicode characters.
    */
    .k-widget {
        font-family: "DejaVu Sans", "Arial", sans-serif;
        font-size: .9em;
    }
</style>

<script>
    // Import DejaVu Sans font for embedding

    // NOTE: Only required if the Kendo UI stylesheets are loaded
    // from a different origin, e.g. cdn.kendostatic.com
    kendo.pdf.defineFont({
        "DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
        "DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
        "DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
        "DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
        "WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
    });
</script>

<!-- Load Pako ZLIB library to enable PDF compression -->
<script src="https://kendo.cdn.telerik.com/2017.3.913/js/pako_deflate.min.js"></script>

<script>
$(document).ready(function() {

    $(".export-pdf").click(function() {
        // Convert the DOM element to a drawing using kendo.drawing.drawDOM
        kendo.drawing.drawDOM($(".content-wrapper"))
        .then(function(group) {
            // Render the result as a PDF file
            return kendo.drawing.exportPDF(group, {
                paperSize: "auto",
                margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
            });
        })
        .done(function(data) {
            // Save the PDF file
            kendo.saveAs({
                dataURI: data,
                fileName: "invoice.pdf",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            });
        });
    });

    $(".export-img").click(function() {
        // Convert the DOM element to a drawing using kendo.drawing.drawDOM
        kendo.drawing.drawDOM($(".content-wrapper"))
        .then(function(group) {
            // Render the result as a PNG image
            return kendo.drawing.exportImage(group);
        })
        .done(function(data) {
            // Save the image file
            kendo.saveAs({
                dataURI: data,
                fileName: "HR-Dashboard.png",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            });
        });
    });

    $(".export-svg").click(function() {
        // Convert the DOM element to a drawing using kendo.drawing.drawDOM
        kendo.drawing.drawDOM($(".content-wrapper"))
        .then(function(group) {
            // Render the result as a SVG document
            return kendo.drawing.exportSVG(group);
        })
        .done(function(data) {
            // Save the SVG document
            kendo.saveAs({
                dataURI: data,
                fileName: "HR-Dashboard.svg",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            });
        });
    });


    var data = [{
      "source": "Approved",
      "percentage": 237
    }, {
      "source": "Rejected",
      "percentage": 112
    }];

    var refs = [{
      "source": "Dev",
      "percentage": 42
    }, {
      "source": "Sales",
      "percentage": 18
    }, {
      "source": "Finance",
      "percentage": 29
    }, {
      "source": "Legal",
      "percentage": 11
    }];

    $("#applications").kendoChart({
      legend: {
        position: "bottom"
      },
      dataSource: {
        data: data
      },
      series: [{
        type: "donut",
        field: "percentage",
        categoryField: "source"
      }],
      chartArea: {
          background: "none"
      },
      tooltip: {
        visible: true,
        template: "${ category } - ${ value } applications"
      }
    });

    $("#users").kendoChart({
        legend: {
            visible: false
        },
        seriesDefaults: {
            type: "column"
        },
        series: [{
            name: "Users Reached",
            data: [340, 894, 1345, 1012, 3043, 2013, 2561, 2018, 2435, 3012]
        }, {
            name: "Applications",
            data: [50, 80, 120, 203, 324, 297, 176, 354, 401, 349]
        }],
        valueAxis: {
            labels: {
                visible: false
            },
            line: {
                visible: false
            },
            majorGridLines: {
                visible: false
            }
        },
        categoryAxis: {
            categories: [2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011],
            line: {
                visible: false
            },
            majorGridLines: {
                visible: false
            }
        },
        chartArea: {
            background: "none"
        },
        tooltip: {
            visible: true,
            format: "{0}",
            template: "#= series.name #: #= value #"
        }
    });

    $("#referrals").kendoChart({
      legend: {
        position: "bottom"
      },
      dataSource: {
        data: refs
      },
      series: [{
        type: "pie",
        field: "percentage",
        categoryField: "source"
      }],
      chartArea: {
          background: "none"
      },
      tooltip: {
        visible: true,
        template: "${ category } - ${ value }%"
      }
    });

    $("#grid").kendoGrid({
      dataSource: {
        type: "odata",
        transport: {
          read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
        },
        pageSize: 15,
        group: { field: "ContactTitle" }
      },
      height: 450,
      groupable: true,
      sortable: true,
      selectable: "multiple",
      reorderable: true,
      resizable: true,
      filterable: true,
      pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
      },
      columns: [
        {
          field: "ContactName",
          template: "<div class=\'customer-name\'>#: ContactName #</div>",
          title: "Contact",
          width: 200
        },{
          field: "ContactTitle",
          title: "Contact Title",
          width: 220
        },{
          field: "Phone",
          title: "Phone",
          width: 160
        },{
          field: "CompanyName",
          title: "Company Name"
        },{
          field: "City",
          title: "City",
          width: 160
        }
      ]
    });
   });
</script>

<style>
.export-app {
    display: table;
    width: 100%;
    height: 750px;
    padding: 0;
}

.export-app .demo-section {
    margin: 0 auto;
    border: 0;
}

.content-wrapper {
    display: table-cell;
    vertical-align: top;
}

.charts-wrapper {
    height: 250px;
    padding: 0 0 20px;
}

#applications,
#users,
#referrals {
    display: inline-block;
    width: 50%;
    height: 240px;
    vertical-align: top;
}
#applications,
#referrals {
    width: 24%;
    height: 250px;
}

.customer-photo {
    display: inline-block;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    background-size: 40px 44px;
    background-position: center center;
    vertical-align: middle;
    line-height: 41px;
    box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
}
.customer-name {
    display: inline-block;
    vertical-align: middle;
    line-height: 41px;
    padding-left: 10px;
}
.demo-section.content-wrapper,
.demo-section.k-content.export-app {
    display: block !important;
}
.demo-section.export-app {
    height: auto;
}
</style>



@endsection