@extends('Admin.Layouts.master')
@section('content')
<div class="card radius-15">
    <div class="card-body">
        <div class="card-title">
        <div class="row">
                <div class="col-md-10">
                    <h4 class="mb-0">View Inquiries</h4>
                </div>
                <div class="col-md-2">

         <a  data-toggle="modal" data-target="#exampleModalen" class="btn btn-light float-right"><i class="fadeIn animated bx bx-money" style="margin-top: -24px;margin-right: 7px;"></i>Add Inquiry</a>
                </div>
            </div>
        </div>
        <hr />

        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Member Id</th>
                        <th> Member Name</th>
                        <th>Month Of </th>
                        <th>Total Amount</th>
                        <th>Paid Amount</th>
                        <th>Balance</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>


                </tbody>

                <tfoot>
                    <tr>
                    <th>Member Id</th>
                        <th> Member Name</th>
                        <th>Month Of </th>
                        <th>Paid Amount</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModalen" tabindex="-1" role="dialog" aria-hidden="true">
    <div id="draggable" class="ui-widget-content">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">Add Enquiry</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label> Name</label>
                            <input class="form-control " type="text" placeholder=" Name" name="member_name">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Father Name</label>
                            <input class="form-control  " type="text" placeholder=" Father Name" name="member_fname">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control  " type="text" placeholder="Email" name="member_phone">
                                                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label> Email</label>
                            <input class="form-control " type="text" placeholder="Email " name="member_name">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Time</label>
                            <input class="form-control  " type="text" placeholder=" Time " name="member_fname">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Reference</label>
                            <input class="form-control  " type="text" placeholder="Reference" name="member_phone">
                                                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label> Cnic</label>
                            <input class="form-control " type="text" placeholder="Cnic " name="member_name">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Age</label>
                            <input class="form-control  " type="text" placeholder="Age" name="member_fname">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input class="form-control  " type="date" placeholder="Date of Birth" name="member_phone">
                                                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Reason of  Joining</label>
                            <input class="form-control " type="text" placeholder="Joining" name="member_name">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Address</label>
                            <input class="form-control  " type="text" placeholder="Address" name="member_fname">
                                                </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Cell</label>
                            <input class="form-control  " type="text" placeholder="Member Phone" name="member_phone">
                                                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label> Package</label>
                            <input class="form-control " type="text" placeholder="Member Name" name="member_name">
                                                </div>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <label>How Many Month Write Amount</label>
                            <input class="form-control  " type="text" placeholder="Member Father Name" name="member_fname">
                                                </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

</div>
@include('Admin.Partials.scripts')

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

  $( function() {
    $( "#draggable" ).draggable();
  } );

</script>
@endsection
