@extends('Admin.Layouts.master')
@section('content')




<!--breadcrumb-->
<div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">
    <div class="breadcrumb-title pr-3">Member</div>
    <div class="pl-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-user"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">View Member</li>
            </ol>
        </nav>
    </div>
</div>
<div class="user-profile-page">
    <div class="card radius-15">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-12 border-right">
                    <div class="d-md-flex align-items-center">
                        <div class="mb-md-0 mb-3">
                            <img src="{{asset('storage/members/'.$member->member_image)}}" class="rounded-circle shadow" width="130"
                                height="130" alt="" />
                        </div>
                        <div class="ml-md-4 flex-grow-1">
                            <div class="d-flex align-items-center mb-1">
                                <h4 class="mb-0">{{ucwords($member->member_name)}}</h4>
                                
                                

                            </div>
                            <p class="mb-0">{{$member->package->package_name}}</p>
                            <p class="mb-0">Member Id :  {{$member->id}}</p>
                            <p class="mb-0">Package Expire :
                               
                            @if(Carbon\Carbon::parse($member->pk_expiry)->format('y-m-d')  < Carbon\Carbon::parse( now())->format('y-m-d')  )
                             <span class="badge badge-pill badge-danger"> Package Expired </span>
                             @else
                          {{   Carbon\Carbon::parse($member->pk_expiry)->format('D | jS \ F, Y')}}
                             @endif
                              
                            </p>
                           

                        </div>
                    </div>
                </div>

            </div>
            <br>
            <!--end row-->


            <div class="" id="Biography">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="card shadow-none border mb-0">
                            <div class="card-body">
                                <h5 class="mb-3">Fee Details</h5>
                                <table class="table">
                                            <tr>
                                                <td>Paid Amount</td>
                                                <td>Month</td>
                                                <td>Balance </td>
                                                <!-- <td>Fee Date </td> -->
                                            </tr>

                                            <tbody>
                                                @foreach($mtrans as $mtran)
                                                <tr>
                                                <td>{{$mtran->fee_amount}}</td>
                                        <td>{{ Carbon\Carbon::parse($mtran->month_of)->format('D, jS \ F ') }}</td>
                                                <td>{{$mtran->member_balance}}</td>
                                                <!-- <td>{{$mtran->member_balance}}</td> -->
                                               </tr>
                                                @endforeach
                                               
                                            </tbody>
</table>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="card shadow-none border mb-0 radius-15">
                            <div class="card-body">



                                <h5 class="mb-3">Member Details</h5>

                                <div class="row">

                                    <div class="col-12 col-lg-6">
                                        <table class="table">
                                            <tr>
                                                <td>Father Name</td>
                                                <td>:</td>
                                                <td>{{$member->member_fname}}</td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td>:</td>
                                                <td>{{$member->member_phone}}</td>
                                            </tr>
                                            <tr>
                                                <td>Cnic</td>
                                                <td>:</td>
                                                <td>{{$member->member_cnic}}</td>
                                            </tr>
                                            <tr>
                                                <td>Reference</td>
                                                <td>:</td>
                                                <td>{{$member->reference}}</td>
                                            </tr>
                                            <tr>
                                                <td>Date Of Join</td>
                                                <td>:</td>
                                                <td>{{ Carbon\Carbon::parse($member->date)->format('D, jS \ F ') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Package</td>
                                                <td>:</td>
                                                <td>{{$member->package->package_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Trainer</td>
                                                <td>:</td>
                                                <td>{{$member->trainer->trainer_name ?? 'No Trainer'}}                                                     </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-12 col-lg-6">
                                        <table class="table">
                                            <tr>
                                                <td>Joining Reason</td>
                                                <td>:</td>
                                                <td>{{$member->reason_of_join}}</td>
                                            </tr>
                                            <tr>
                                                <td>Medical History</td>
                                                <td>:</td>
                                                <td>{{$member->medical_history}}</td>
                                            </tr>
                                            <tr>
                                                <td>Timing</td>
                                                <td>:</td>
                                                <td>{{$member->member_timings}}</td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td>:</td>
                                                <td>{{$member->address}}</td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td>:</td>
                                                <td> @if ($member->member_status == 1)
                            <span class="badge badge-pill badge-success">Active</span>
                                @else
                                <span class="badge badge-pill badge-info">Freeze</span>
                            @endif</td>
                                            </tr>
                                            <tr>
                                                <td>Total Fee</td>
                                                <td>:</td>
                                                <td>{{number_format($member->total)}}</td>
                                            </tr>


                                            <tr>
                                                <td>Balance</td>
                                                <td>:</td>
                                                <td>{{number_format($member->balance)}}</td>
                                            </tr>
                                          
                                           
                                        </table>
                                    </div>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endsection