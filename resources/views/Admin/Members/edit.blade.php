@extends('Admin.Layouts.master')
@section('content')

<style>
    .modal-dialog {
        margin-top: 100px;
    }

    .card {
        margin-bottom: 8px;
    }

    .fade:not(.show) {
        opacity: 1;
    }

    #create-package {
        display: block;
    }
</style>


<!--breadcrumb-->
<div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">
    <div class="breadcrumb-title pr-3">Member Form</div>
    <div class="pl-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-user"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Add Member</li>
            </ol>
        </nav>
    </div>
</div>
<!--end breadcrumb-->
<div class="card radius-15">
    <div class="card-body">
        <div class="card-title">
            <h4 class="mb-0">Edit Member</h4>
        </div>
        <hr />
        <form action="{{route('admin.member.update')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <input type="hidden" value="{{$member->id}}" name="member_id">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member Name</label>
                        <input class="form-control" type="text" placeholder="Member Name" name="member_name" value="{{$member->member_name}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member Father Name</label>
                        <input class="form-control" type="text" placeholder="Member Father Name" name="member_fname"  value="{{$member->member_fname}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member Phone</label>
                        <input class="form-control" type="text" placeholder="Member Phone" name="member_phone"  value="{{$member->member_phone}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member CNIC</label>
                        <input class="form-control" type="text" placeholder="Member CNIC" name="member_cnic"  value="{{$member->member_cnic}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select Refrence</label>
                        <select class="form-control" type="text" name="member_reference" >
                            
                        @php
                                $ref = ['Facebook','Instagram','Website'];
                            @endphp
                                    @for ($k = 0 ; $k < count($ref); $k++)
                                    @if ($ref[$k] == $member->reference)
                                    <option  selected value="{{$ref[$k]}}">{{$ref[$k]}}</option>   
                                    @else
                                    <option value="{{$ref[$k]}}">{{$ref[$k]}}</option>      
                                    @endif     
                                    
                                    @endfor

                           
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date Of Birth</label>
                        <input type="date" class="form-control datepicker" name="member_dob"  value="{{$member->date}}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label> Reason of joining</label>
                        <input class="form-control" type="text" placeholder="Member Reason of joining" name="member_roj"  value="{{$member->reason_of_join}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Medical History </label>
                        <select class="form-control" type="text" name="member_medical">
                            @php
                                $me = ['Yes','No'];
                            @endphp
                                    @for ($k = 0 ; $k < count($me); $k++)
                                    @if ($me[$k] == $member->medical_history)
                                    <option  selected value="{{$me[$k]}}">{{$me[$k]}}</option>   
                                    @else
                                    <option value="{{$me[$k]}}">{{$me[$k]}}</option>      
                                    @endif     
                                    
                                    @endfor
                            
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Timings Slot</label>
                        @php
                            $am = ['12 to 1','1 to 2','2 to 3','3 to 4','4 to 5','5 to 6','6 to 7','7 to 8','8 to 9','9 to 10','10 to 11','11 to 12'];
                             
                            
                            
                        @endphp
                        <select class="form-control" type="text" name="member_timings" value="{{$member->member_timings}}"> 
                            @php
                                for($i = 0 ; $i < count($am); $i++){
                                    if($member->member_timings == $am[$i]){
                                        echo('<option selected value="'.$am[$i].'">'.$am[$i] .'</option>'); 
                                    }
                                    echo('<option   value="'.$am[$i].'">'.$am[$i] .'AM</option>'); 

                                }
                                
                            @endphp

                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Timings Slot</label>
                      
                        <select class="form-control  @error('gender') is-invalid  @enderror" type="text" name="gender">
                        @php
                                $gender = ['Male','Female'];
                            @endphp
                            @php
                                for($i = 0 ; $i < count($gender); $i++){
                                    if($member->member_gender == $gender[$i]){
                                        echo('<option selected value="'.$gender[$i].'">'.$gender[$i] .'</option>'); 
                                    }
                                    echo('<option   value="'.$gender[$i].'">'.$gender[$i] .'</option>'); 
                                }
                                
                            @endphp
                        </select>
                        @error('gender')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Member Address</label>
                        <input class="form-control" type="text" placeholder="Member Address" name="member_adress"  value="{{$member->address}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Select Package</label>
                        <select class="form-control" id="package" type="text" onchange="total();uncheck()" name="member_package">
                            @foreach ($active_package as $apackage)
                            @if ($member->package_id == $apackage->id)
                            <option value="{{$apackage->id}}" selected>{{$apackage->package_name}}</option>
                            @else
                            <option value="{{$apackage->id}}">{{$apackage->package_name}}</option>
                            @endif
                           
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Personal Trainer</label>
                        <select class="form-control" id="trainer" type="text" onchange="total();uncheck()" name="member_trainer">
                            <option  value="">No  Trainer</option>
                            @foreach ($active_trainer as $atrainer)
                            @if ($member->trainer_id == $atrainer->id)
                            <option value="{{$atrainer->id}}" selected>{{$atrainer->trainer_name}}</option>
                            @else
                            <option value="{{$atrainer->id}}">{{$atrainer->trainer_name}}</option>
                            @endif
                            @endforeach
                         
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Registration fee</label>
                                <input class="form-control" type="text" id="reg_fee" readonly placeholder="Registration fee" name="member_regfeees">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Add fee</label>
                                <input type="checkbox" name="member_regfee" class="form-control" type="text" id="add_fee" onclick="add_data()" readonly placeholder="Registration fee"
                                {{$member->member_regfee == 1 ? 'checked' : ''}}>

                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Total fee</label>
                                <input name="member_totalfee" class="form-control" type="text" id="total_amount" placeholder="Total" readonly  value="">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <label>Upload Image</label>
                    <!-- <input type="hidden" name="old_img" value="{{$member->member_image}}"> -->
                    <div class="custom-file">
                        <input name="member_image" type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile01"></label>
                        <!-- <img src="{{asset('storage/members/'.$member->member_image)}}" alt="" > -->
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                    <label>Member Status</label>
                    
                    <div class="form-group">
                        <select id="st" onchange="status()" class="form-control" name="member_status"  alt="" > 
                            
                                
                                <option @if ( $member->member_status == 1) selected @endif value="1">Active</option>
                                <option @if ( $member->member_status == 2)selected @endif value="2">Freez</option>
                                <option @if ( $member->member_status == 3)selected @endif value="3">De Active</option>
                                 
                                
                    </select>
                    </div>
                </div>
            </div>
            <div class="row" id="freeze_date" style="display: none;">
            <div class="col-md-12">
                    <label>Freez Date</label>
                   <input type="date" class="form-control" name="freez_date" value="{{$member->freeze_date}}">  
                    <div class="form-group">
                    </div>
                    </div>
                    </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-light float-right"> <label><i class="bx bx-user"></i></label>
                            Update Member</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@include('Admin.Partials.scripts')
@if(Session::has('success'))
<script>
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        }
        toastr.success("{{ Session::get('success') }}");
</script>
@endif
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('#example1').DataTable();
        var table = $('#example2').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
        });
        table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
    });

function uncheck(){
    $('#add_fee').prop('checked', false)
}

    
    function total() {
        
      
        var package = document.getElementById('package');
        var trainer = document.getElementById('trainer');
        var total_amount = document.getElementById('total_amount');
        var reg_fee = document.getElementById('reg_fee');

        if (package.value != '' || trainer.value != '') {
         
            $.ajax({
                url: '{{route("admin.member.packgaetrainer")}}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "package_id": package.value,
                    "trainer_id": trainer.value
                },
                success: function(res) {
                    total_amount.value = res.total;
                    reg_fee.value = res.reg_fees;
                  
                   
                    
                }
            })
        }
    }
    function add_data() {

        var reg_fee = document.getElementById('reg_fee');
        var total_amount = document.getElementById('total_amount');
        var add_fee = document.getElementById('add_fee');
        var status = 0;
 
        var amount = total_amount.value;
        console.log(total_amount.value)
       
 
            $.ajax({
                url: '{{route("admin.member.packgaetrainer")}}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "package_id": package.value,
                    "trainer_id": trainer.value
                },
                success: function(res) {
              
                    if (add_fee.checked) {
                    total_amount.value = parseInt(res.total) + parseInt(res.reg_fees); 
                    console.log(res);
                    }
                    else{
                        console.log(parseInt(res.total))
                        console.log(parseInt(res.reg_fees))
                        total_amount.value =   parseInt(total_amount.value)  - parseInt(res.reg_fees) ; 
                    }
                
                }
            })

       
    }
   
    
        function status(){
            var st = document.getElementById('st');
            if(st.value == 2){
                document.getElementById('freeze_date').style.display="block";
            }
            else{
                document.getElementById('freeze_date').style.display="none";
            }
        }
        total()
    add_data()
    status()
</script>
@endsection