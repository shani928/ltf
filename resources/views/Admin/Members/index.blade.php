@extends('Admin.Layouts.master')
@section('content')

<div class="card radius-15">
    <div class="card-body">
        <div class="card-title">
            <div class="row">
                <div class="col-md-10">
                    <h4 class="mb-0">View Member</h4>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-light" href="{{route('admin.member.create')}}">+ Add Member</a>
                </div>
            </div>
        </div>
        <hr />

        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                    <th> Id</th>
                        <th> Name</th>
                        <th>Package </th>
                        <th>Phone</th>
                        <th>DOJ</th>
                        <th>Timing</th>
                        <th>Status</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($member as $members)
                    <tr class="members_id-{{$members->id}}">
                    <td>{{$members->id}}</td>
                        <td>{{$members->member_name}}</td>
                        <td>{{$members->package->package_name}}</td>
                        <td>{{$members->member_phone}}</td>
                        <td>{{ Carbon\Carbon::parse($members->date)->format('D, jS \ F ') }}</td>
                        <td>{{$members->member_timings}}</td>
                        <td>
                            @if ($members->member_status == 1)
                            <span class="badge badge-pill badge-success">Active</span>
                                @elseif($members->member_status == 2)
                                <span class="badge badge-pill badge-info">Freeze</span>
                                @elseif($members->member_status == 3)
                                <span class="badge badge-pill badge-danger">De Active</span>

                            @endif
                        </td>
                        <td>
                            @if($members->member_image != '')
                            <img src="{{asset('storage/members/'.$members->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif

                        </td>
                        <td>
                            <a role="{{$members->id}}" style="padding: 4px;" class="deletemem btn btn-danger"><i class="lni lni-trash"></i></a>
                            <a style="padding: 4px;" href="{{route('admin.member.edit',$members->id)}}" class="btn btn-info"><i class="bx bx-comment-edit"></i></a>
                            <a href="{{route('admin.member.view',$members->id)}}" style="padding: 4px;" class="btn btn-success"><i class="lni lni-eye"></i>
                        </td>
                    </tr>

                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                    <th> Id</th>
                        <th> Name</th>
                        <th>Package </th>
                        <th>Phone</th>
                        <th>Date Of Joining</th>
                        <th> Timing</th>  
                        <th>Status</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@include('Admin.Partials.scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<script>
    var del = document.getElementsByClassName('deletemem');
    for (let i = 0; i < del.length; i++) {
        del[i].addEventListener('click', function() {
            var mem_id = this.getAttribute('role');
            var row_id = $(this).attr('members_id');
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Member!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        var data = {
                            "_token": "{{ csrf_token() }}",
                            "mem_id": mem_id,
                        };

                        $.ajax({
                            type: "POST",
                            url: "{{route('admin.member.delete')}}",
                            data: data,
                            success: function(response) {
                                swal(response.status, {
                                        icon: "success",
                                    })
                                    .then((result) => {
                                        $(".members_id-" + mem_id).remove();
                                    });
                            }
                        })
                    }
                });
        });
    }
</script>
@endsection