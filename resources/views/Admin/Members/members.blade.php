@extends('Admin.Layouts.master')
@section('content')

<style>
    .modal-dialog {
        margin-top: 100px;
    }

    .card {
        margin-bottom: 8px;
    }

    .fade:not(.show) {
        opacity: 1;
    }

    #create-package {
        display: block;
    }
</style>


<!--breadcrumb-->
<div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">
    <div class="breadcrumb-title pr-3">Member</div>
    <div class="pl-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-user"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Add Member</li>
            </ol>
        </nav>
    </div>
    <div class="ml-auto">
        <div class="btn-group">
            <button type="button" class="btn btn-light" data-toggle="modal" data-target="#add">+ Add Package & Trainer </button>


        </div>
    </div>
</div>
<!--end breadcrumb-->
<div class="card radius-15">
    <div class="card-body">
        <div class="card-title">
            <h4 class="mb-0">Add Member</h4>
        </div>
        <hr />
        <form action="{{route('admin.member.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member Name</label>
                        <input class="form-control @error('member_name') is-invalid  @enderror" type="text" placeholder="Member Name" name="member_name">
                        @error('member_name')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member Father Name</label>
                        <input class="form-control  @error('member_fname') is-invalid  @enderror" type="text" placeholder="Member Father Name" name="member_fname">
                        @error('member_fname')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member Phone</label>
                        <input class="form-control  @error('member_phone') is-invalid  @enderror" type="text" placeholder="Member Phone" name="member_phone">
                        @error('member_phone')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member CNIC</label>
                        <input class="form-control  @error('member_cnic') is-invalid  @enderror" type="text" placeholder="Member CNIC" name="member_cnic">
                        @error('member_cnic')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select Refrence</label>
                        <select class="form-control  @error('member_reference') is-invalid  @enderror" type="text" name="member_reference">
                            <option selected disabled>Select Refrence </option>
                            <option value="Facebook">Facebook</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Website">Website</option>
                        </select>
                        @error('member_reference')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date Of Joining</label>
                        <input type="date" class="form-control datepicker  @error('member_dob') is-invalid  @enderror" name="member_dob" />
                        @error('member_dob')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label> Reason of joining</label>
                        <input class="form-control  @error('member_roj') is-invalid  @enderror" type="text" placeholder="Member Reason of joining" name="member_roj">
                        @error('member_roj')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Medical History </label>
                        <select class="form-control  @error('member_medical') is-invalid  @enderror" type="text" name="member_medical">
                            <option selected>Medical History</option>
                            <option>Yes</option>
                            <option>No</option>
                        </select>
                        @error('member_medical')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Timings Slot</label>
                        @php
                        $am = ['12 to 1','1 to 2','2 to 3','3 to 4','4 to 5','5 to 6','6 to 7','7 to 8','8 to 9','9 to 10','10 to 11','11 to 12'];
                        @endphp
                        <select class="form-control  @error('member_timings') is-invalid  @enderror" type="text" name="member_timings">
                            <option selected disabled>Timings Slot</option>
                            @php
                            for($i = 0 ; $i < count($am); $i++){ echo('<option value="'.$am[$i].'">'.$am[$i] .' AM</option>');
                                }
                                @endphp
                        </select>
                        @error('member_timings')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Timings Slot</label>

                        <select class="form-control  @error('gender') is-invalid  @enderror" type="text" name="gender">
                            <option selected disabled>Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        @error('gender')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Member Address</label>
                        <input class="form-control  @error('member_adress') is-invalid  @enderror" type="text" placeholder="Member Address" name="member_adress">
                        @error('member_adress')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select Package</label>
                        <select class="form-control  @error('member_package') is-invalid  @enderror" id="package" type="text" onchange="total()" name="member_package">
                            <option selected disabled value="">Select Package</option>
                            @foreach ($active_package as $apackage)
                            <option value="{{$apackage->id}}">{{$apackage->package_name}}</option>
                            @endforeach
                        </select>
                        @error('member_package')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Package Start Date </label>
                        <input class="form-control  @error('pk_start') is-invalid  @enderror" type="date" name="pk_start">
                        @error('pk_start')
                        <div class="text text-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Personal Trainer</label>
                        <select class="form-control" id="trainer" type="text" onchange="total()" name="member_trainer">
                            <option selected disabled value="">Select Personal Trainer</option>
                            @foreach ($active_trainer as $atrainer)
                            <option value="{{$atrainer->id}}">{{$atrainer->trainer_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Registration fee</label>
                                <input class="form-control" type="text" id="reg_fee" readonly placeholder="Registration fee" name="member_regfeees">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Add Registertion Fee?</label>
                                <input type="checkbox" name="member_regfee" id="add_fee" onclick="add_data()" readonly placeholder="Registration fee">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Total fee</label>
                                <input name="member_totalfee" class="form-control" type="text" id="total_amount" placeholder="Total" readonly>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <label>Upload Image</label>
                    <div class="custom-file">
                        <input name="member_image" type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile01"></label>
                    </div>
                </div>
            </div>


            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-light float-right"> <label><i class="bx bx-user"></i></label>
                            Add Member</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>



<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="div-head">Create Package</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body  bg-theme bg-theme1 pace-done" style="background-image: url(http://127.0.0.1:8000/fitnessback/assets/images/bg-themes/1.png) !important">
                <div class="row">
                    <div class="col-3 pr-0">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <div class="col-12 col-lg-12">
                                <div class="card radius-15">
                                    <div class="card-body" id="tab1">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <h4 class="mb-0 font-weight-bold"> Create Package </h4>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-12">
                                <div class="card radius-15">
                                    <div class="card-body" id="tab2">
                                        <div class="media align-items-center">
                                            <div class="media-body ">
                                                <h4 class="mb-0 font-weight-bold">

                                                    View
                                                    Package

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-12">
                                <div class="card radius-15">
                                    <div class="card-body" id="tab3">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <h4 class="mb-0 font-weight-bold">

                                                    Add Trainer

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-12">
                                <div class="card radius-15">
                                    <div class="card-body" id="tab4">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <h4 class="mb-0 font-weight-bold">
                                                    View Trainer

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 pl-0" style="border:1px solid rgb(255 255 255 / 14%)">
                        <div class="tab-content p-10" id="">

                            <div class="tab-pane fade " id="create-package">
                                <form id="pkform">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Package Name</label>
                                                <input class="form-control" type="text" placeholder="Package Name" id="pkname" name="pkname">
                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Package Duration</label>
                                                <select class="form-control" id="pkduration" name="pkduration">
                                                    <option value="">Select Duration</option>
                                                    <option value="1">1 Month</option>
                                                    <option value="2">2 Month</option>
                                                    <option value="3">3 Month</option>
                                                    <option value="4">4 Month</option>
                                                    <option value="5">5 Month</option>
                                                    <option value="6">6 Month</option>
                                                    <option value="7">7 Month</option>
                                                    <option value="8">8 Month</option>
                                                    <option value="9">9 Month</option>
                                                    <option value="10">10 Month</option>
                                                    <option value="11">11 Month</option>
                                                    <option value="12">12 Month</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label>Package Amount</label>
                                                <input class="form-control" type="text" placeholder="Package Amount" id="pkamount" name="pkamount">
                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Package Status</label>
                                                <select class="form-control" id="pkstatus" name="pkstatus">
                                                    <option value="">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">De-Active</option>
                                                </select>
                                            </div>
                                            <input type="hidden" id="pkid">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Registeration Fee</label>
                                                <input class="form-control" type="number" placeholder="Registeration Fee" id="reg_fees" name="reg_fees">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" id="pkbtn">Save Package </button>
                                    </div>
                            </div>
                            </form>
                            <div class="tab-pane fade" id="view-package">
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Package Name</th>
                                                <th>Package Duration</th>
                                                <th>Package Amount</th>
                                                <th>Status</th>
                                                <th>Registeration Fee</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($package as $packages)
                                            <tr class="package_id-{{$packages->id}}">
                                                <td>{{$packages->package_name}}</td>
                                                <td>{{$packages->package_duration}}</td>
                                                <td>{{$packages->package_amount}}</td>
                                                <td>
                                                    @if ($packages->status == 1)
                                                    <span class="badge badge-pill badge-success">Active</span>
                                                    @else
                                                    <span class="badge badge-pill badge-danger">De-Active</span>
                                                    @endif
                                                </td>
                                                <td>{{$packages->reg_fees}}</td>
                                                <td>
                                                    <button style="padding: 4px;" role="{{$packages->id}}" type="button" class="deletepk btn btn-danger"><i class="lni lni-trash"></i></button>
                                                    <button style="padding: 4px;" onclick="package_edit({{$packages->id}})" role="{{$packages->id}}" type="button" class="btn btn-info"><i class="bx bx-comment-edit"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Package Name</th>
                                                <th>Package Duration</th>
                                                <th>Package Amount</th>
                                                <th>Status</th>
                                                <th>Registeration Fee</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="add-trainer">
                                <form id="trform" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input class="form-control" type="text" placeholder="Full Name" id="trname" name="trname">
                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Gender</label>
                                                <select class="form-control" type="text" id="trgender" name="trgender">
                                                    <option selected disabled>Select Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">FeMale</option>
                                                </select>
                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Dob</label>
                                                <input class="form-control" type="date" placeholder="Date Of Birth" id="trdob" name="trdob">
                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Expertise</label>
                                                <input class="form-control" type="text" placeholder="Expertise" id="trexpert" name="trexpert">

                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Personal training fee</label>
                                                <input class="form-control" type="number" placeholder="Personal training fee" id="trfee" name="trfee">
                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <label>Upload Image</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="trimage" name="trimage">
                                                    <label class="custom-file-label" for="inputGroupFile01"></label>
                                                </div>
                                                <input type="hidden" id="hiddenimg">
                                                <input type="hidden" id="trid" name="trid">

                                                <div class="invalid-tooltip">Please provide a valid city.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-light" id="trbtn">Save Trainer </button>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade" id="view-trainer">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Trainer Name</th>
                                                <th>Trainer Fee</th>
                                                <th>Trainer Expertise</th>
                                                <th>Gender</th>
                                                <th>Trainer Image</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($trainer as $trainers)
                                            <tr class="trainer_id-{{$trainers->id}}">
                                                <td>{{$trainers->trainer_name}}</td>
                                                <td>{{$trainers->trainer_fee}}</td>
                                                <td>{{$trainers->expertise}}</td>
                                                <td> {{$trainers->gender}}</td>
                                                <td>
                                                    @if ($trainers->trainer_image != '')
                                                    <img src="{{asset('storage/trainers/'.$trainers->trainer_image)}}" height="50px" width="50px" alt="">
                                                    @else
                                                    <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                                                    @endif
                                                </td>
                                                <td>
                                                    <button style="padding: 4px;" type="button" role="{{$trainers->id}}" class="deletetr btn btn-danger"><i class="lni lni-trash"></i></button>
                                                    <a style="padding: 4px;" onclick="trainer_edit({{$trainers->id}})" class="btn btn-info"> <i class="bx bx-comment-edit"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
@include('Admin.Partials.scripts')
@if(Session::has('success'))
<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
    toastr.success("{{ Session::get('success') }}");
</script>
@endif
<script>
    var del = document.getElementsByClassName('deletepk');
    for (let i = 0; i < del.length; i++) {
        del[i].addEventListener('click', function() {
            var package_id = this.getAttribute('role');
            var row_id = $(this).attr('package_id');
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Package!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        var data = {
                            "_token": "{{ csrf_token() }}",
                            "package_id": package_id,
                        };

                        $.ajax({
                            type: "POST",
                            url: "{{route('admin.member.deletePackage')}}",
                            data: data,
                            success: function(response) {
                                swal(response.status, {
                                        icon: "success",
                                    })
                                    .then((result) => {
                                        $(".package_id-" + package_id).remove();
                                    });
                            }
                        })
                    }
                });
        });
    }
</script>
<script>
    var del_tr = document.getElementsByClassName('deletetr');
    for (i = 0; i < del_tr.length; i++) {
        del_tr[i].addEventListener('click', function() {
            var trainer_id = this.getAttribute('role');
            var row_id = $(this).attr('trainer_id');
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Trainer!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })

                .then((willDelete) => {
                    if (willDelete) {

                        var data = {
                            "_token": "{{ csrf_token() }}",
                            "trainer_id": trainer_id,
                        };

                        $.ajax({
                            type: "POST",
                            url: "{{route('admin.member.deleteTrainer')}}",
                            data: data,
                            success: function(response) {
                                swal(response.status, {
                                        icon: "success",
                                    })
                                    .then((result) => {
                                        $(".trainer_id-" + trainer_id).remove();
                                    });
                            }
                        })
                    }
                });

        });
    }
</script>
<script>
    $('#tab1').click(function() {
        $('#pkform').trigger("reset");
        $('#create-package').show();
        $('#view-package').hide();
        $('#add-trainer').hide();
        $('#view-trainer').hide();
        $('#div-head').empty().append('Create Package');
    });
    $('#tab2').click(function() {
        $('#create-package').hide();
        $('#view-package').show();
        $('#add-trainer').hide();
        $('#view-trainer').hide();
        $('#div-head').empty().append('View Package');
    });
    $('#tab3').click(function() {
        $('#trform').trigger("reset");
        $('#create-package').hide();
        $('#view-package').hide();
        $('#add-trainer').show();
        $('#view-trainer').hide();
        $('#div-head').empty().append('Add  Trainer');
    });
    $('#tab4').click(function() {
        $('#create-package').hide();
        $('#view-package').hide();
        $('#add-trainer').hide();
        $('#view-trainer').show();
        $('#div-head').empty().append('View Trainer');
    });

    function trainer_edit(a) {

        $('#create-package').hide();
        $('#view-package').hide();
        $('#add-trainer').show();
        $('#view-trainer').hide();
        $.ajax({
            url: '{{route("admin.member.editTrainer")}}',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "data": {
                    id: a
                },
            },
            success: function(res) {
                console.log(res);
                document.getElementById('trname').value = res[0]['trainer_name']
                document.getElementById('trgender').value = res[0]['gender']
                document.getElementById('trdob').value = res[0]['dob']
                document.getElementById('trexpert').value = res[0]['expertise']
                document.getElementById('trfee').value = res[0]['trainer_fee']
                document.getElementById('hiddenimg').value = res[0]['trainer_image']
                document.getElementById('trid').value = res[0]['id']

            }
        })
    }

    function package_edit(a) {


        $.ajax({
            url: '{{route("admin.member.viewPackage")}}',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "data": {
                    id: a
                },
            },
            success: function(res) {

                document.getElementById('pkname').value = res[0]['package_name']
                document.getElementById('pkduration').value = res[0]['package_duration']
                document.getElementById('pkamount').value = res[0]['package_amount']
                document.getElementById('pkstatus').value = res[0]['status']
                document.getElementById('reg_fees').value = res[0]['reg_fees']
                document.getElementById('pkid').value = res[0]['id']


            }
        })
        $('#create-package').show();
        $('#view-package').hide();
        $('#add-trainer').hide();
        $('#view-trainer').hide();
    }
</script>
<script>
    document.getElementById('pkbtn').addEventListener('click', function() {
        $('#pkbtn').empty().append('<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>');
        var data = {
            'pkname': document.getElementById('pkname').value,
            'pkduration': document.getElementById('pkduration').value,
            'pkamount': document.getElementById('pkamount').value,
            'pkstatus': document.getElementById('pkstatus').value,
            'reg_fees': document.getElementById('reg_fees').value,
            'pkid': document.getElementById('pkid').value,
        };

        console.log(data);
        $.ajax({
            url: '{{route("admin.member.storePackage")}}',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "data": data,
            },
            success: function(res) {
                swal(res.status, {
                    icon: "success",
                });
                $('#pkbtn').empty().append('Save Package');
                if (res.status == 'Package Created Successfully!') {
                    document.getElementById("pkform").reset();
                }

            }
        })
    });
</script>
<script>
    $('#trform').on('submit', function(event) {
        event.preventDefault();
        $('#trbtn').empty().append('<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>')
        $.ajax({
            url: '{{route("admin.member.storeTrainer")}}',
            type: 'POST',
            data: new FormData(this),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                swal(response.status, {
                    icon: "success",
                });
                $('#trbtn').empty().append('Save Trainer');
                document.getElementById("trform").reset();
            }
        })

    });
</script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('#example1').DataTable();
        var table = $('#example2').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
        });
        table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
    });


    function total() {
        var package = document.getElementById('package');
        var trainer = document.getElementById('trainer');
        var total_amount = document.getElementById('total_amount');
        var reg_fee = document.getElementById('reg_fee');

        if (package.value != '' || trainer.value != '') {
            $.ajax({
                url: '{{route("admin.member.packgaetrainer")}}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "package_id": package.value || 0,
                    "trainer_id": trainer.value || 0
                },
                success: function(res) {
                    total_amount.value = res.total;
                    reg_fee.value = res.reg_fees;
                    console.log(res);
                }
            })
        }

    }

    function add_data() {

        var reg_fee = document.getElementById('reg_fee');
        var total_amount = document.getElementById('total_amount');
        var add_fee = document.getElementById('add_fee');
        var status = 0;
        var amount = total_amount.value;
        if (add_fee.checked) {

            total_amount.value = (parseInt(reg_fee.value) + parseInt(amount));
        } else {

            total_amount.value = (parseInt(amount) - parseInt(reg_fee.value));


        }

    }
    $( "#add").draggable();
</script>

@endsection
