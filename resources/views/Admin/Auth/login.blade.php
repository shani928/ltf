<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    @include('Admin.Partials.styles')
</head>

<body class="bg-theme bg-theme1">
    <!-- wrapper -->
    <div class="wrapper">
        <div class="section-authentication-login d-flex align-items-center justify-content-center">
            <div class="row">
                <div class="col-12 col-lg-10 mx-auto">
                    <div class="card radius-15">
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="card-body p-md-5">
                                    <div class="text-center">
                                        <img src="{{asset('fitnessback/assets/images/logo-icon.png')}}" width="80" alt="">
                                        <h3 class="mt-4 font-weight-bold">Welcome Back</h3>
                                    </div>
                                    <!-- <div class="input-group shadow-sm rounded mt-5">
										<div class="input-group-prepend">	<span class="input-group-text cursor-pointer"><i class='bx bxl-google' ></i></span>
										</div>
										<input type="button" class="form-control" value="Log in with google">
									</div> -->
                                    <div class="login-separater text-center"> <span> LOGIN WITH EMAIL</span>
                                        <hr />
                                    </div>
                                    <form action="{{route('admin.loginstore')}}" method="POST">
                                        @csrf
                                        <div class="form-group mt-4">
                                            <label>Email Address</label>
                                            <input type="email" name="email" class="form-control @error('email') is-invalid  @enderror" placeholder="Enter your email address" />
                                            @error('email')
                                         <span class="text text-danger">{{$message}}</span> 
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control  @error('password') is-invalid  @enderror" placeholder="Enter your password" />
                                            @error('password')
                                            <div class="text text-danger"> {{$message}}</div>
                                            @enderror
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input" id="customSwitch1" checked>
                                                    <label class="custom-control-label" for="customSwitch1">Remember Me</label>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group col text-right"> <a href="authentication-forgot-password.html"><i class='bx bxs-key mr-2'></i>Forget Password?</a>
										</div> -->
                                        </div>
                                        <div class="btn-group mt-3 w-100">
                                            <button type="submit" class="btn btn-light btn-block">Log In</button>
                                            <button type="submit" class="btn btn-light"><i class="lni lni-arrow-right"></i>
                                            </button>
                                        </div>
                                        <hr>
                                        <!-- <div class="text-center">
										<p class="mb-0">Don't have an account? <a href="authentication-register.html">Sign up</a>
										</p>
									</div> -->
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <img src="{{asset('fitnessback/assets/images/login-images/login-frent-img.jpg')}}" class="card-img login-img h-100" alt="...">
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('Admin.Partials.scripts')
@if(Session::has('error'))
<script>
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        }
        toastr.error("{{ Session::get('error') }}");
</script>
@endif
</body>
</html>