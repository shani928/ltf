@extends('Admin.Layouts.master')
@section('content')
    <link href="{{asset('fitnessback/assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
	<link href="{{asset('fitnessback/assets/plugins/apexcharts-bundle/css/apexcharts.css')}}" rel="stylesheet" />
<div class="row">
    <div class="col-12 col-lg-3">
        <div class="card radius-15">
            <a href="{{route('admin.member.index')}}">
                <div class="card-body">
                    <div class="media align-items-center">
                        <div class="media-body">
                            <h4 class="mb-0 font-weight-bold text-white">{{$dashboard[0]['All_members']}}</h4>
                            <p class="mb-0 text-white">Total Members</p>
                        </div>
                        <div class="font-35 text-white"><i class="bx bx-group"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-12 col-lg-3">
        <div class="card radius-15">
            <div class="card-body">
                <a data-toggle="modal" data-target="#exampleModal7">
                    <div class="media align-items-center">
                        <div class="media-body">
                            <h4 class="mb-0 font-weight-bold text-white">{{$dashboard[0]['All_active_members']}}</h4>
                            <p class="mb-0">Active Members</p>
                        </div>
                        <div class="font-35 text-white"><i class="bx bx-group"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-3">
        <div class="card radius-15">
            <div class="card-body">
                <a data-toggle="modal" data-target="#exampleModal8">
                    <div class="media align-items-center">
                        <div class="media-body">
                            <h4 class="mb-0 font-weight-bold text-white">{{$dashboard[0]['All_freez_members']}}</h4>
                            <p class="mb-0 text-white">Freze Members</p>
                        </div>
                        <div class="font-35 text-white"><i class="bx bx-group"></i>
                        </div>
                    </div>
            </div>
            </a>
        </div>
    </div>
    <div class="col-12 col-lg-3">
        <div class="card radius-15">
            <div class="card-body">
                <a data-toggle="modal" data-target="#exampleModal9">
                    <div class="media align-items-center">
                        <div class="media-body">
                            <h4 class="mb-0 font-weight-bold text-white">{{$dashboard[0]['trainer']}}</h4>
                            <p class="mb-0 text-white">Trainers</p>
                        </div>
                        <div class="font-35 text-white"><i class="bx bx-group"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card-deck flex-column flex-lg-row">
            <div class="card radius-15">
                <div class="card-body">
                    <div class="card-title">
                        <h5 class="mb-0">Members Info</h5>
                    </div>

                    <hr>
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Male</p>
                        </div>
                        <p class="mb-0">{{$dashboard[0]['male_members']}}</p>
                    </div>
                    <hr>



                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Female</p>
                        </div>
                        <p class="mb-0">{{$dashboard[0]['female_members']}}</p>
                    </div>
                    <hr>

                    @foreach($dashboard[0]['members_packages'] as $package)
                    <input type="hidden" value="{{$package->package_name}}" class="package_name">
                    <input type="hidden" value="{{$package->total}}" class="package_total">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">{{$package->package_name}}</p>
                        </div>
                        <p class="mb-0">{{$package->total}}</p>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
            <div class="card radius-15">
                <div class="card-body">
                    <div class="card-title">
                        <h5 class="mb-0">Earning's </h5>
                    </div>

                    <hr>
                    <a data-toggle="modal" data-target="#exampleModal10">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Today</p>
                        </div>

                        <p class="mb-0"> {{ number_format(($dashboard[0]['today_amount'][0]->Today),0)}} </p>
                    </div>
</a>

                    <hr>


                    <a data-toggle="modal" data-target="#exampleModal11">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Last 15 Days</p>
                        </div>
                        <p class="mb-0">{{ number_format(($dashboard[0]['fiftyday_amount'][0]->FiftenDays),0)}}</p>
                    </div>
</a>
                    <hr>
                    <a data-toggle="modal" data-target="#exampleModal12">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Monthly</p>
                        </div>
                        <p class="mb-0">{{ number_format(($dashboard[0]['monthly_amount'][0]->Monthly),0)}}</p>
                    </div>
</a>
                    <hr>
                    <a data-toggle="modal" data-target="#exampleModal6">
                        <div class="media align-items-center">
                            <div>

                            </div>

                            <div class="media-body ml-3">

                                <p class="mb-0">Balance</p>
                            </div>
                            <p class="mb-0">{{ number_format(($dashboard[0]['balance']),0)}}</p>
                        </div>
                    </a>
                    <hr>
                </div>
            </div>
            <div class="card radius-15">
                <div class="card-body" style="position: relative;">
                    <div class="card-title">
                        <h5 class="mb-0">Package Expiry </h5>
                    </div>
                    <hr>
                    <a data-toggle="modal" data-target="#exampleModal13">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Today</p>
                        </div>
                        <p class="mb-0">{{ number_format(($dashboard[0]['today_expire'][0]->Today),0)}}</p>
                    </div>
</a>
                    <hr>
                    <a data-toggle="modal" data-target="#exampleModal14">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Last 15 Days</p>
                        </div>
                        <p class="mb-0">{{ number_format(($dashboard[0]['fiftyday_expire'][0]->FiftenDays),0)}}</p>
                    </div>
</a>
                    <hr>
                    <a data-toggle="modal" data-target="#exampleModal15">
                    <div class="media align-items-center">
                        <div>

                        </div>
                        <div class="media-body ml-3">

                            <p class="mb-0">Monthly</p>
                        </div>
                        <p class="mb-0">{{ number_format(($dashboard[0]['monthly_expire'][0]->Monthly),0)}}</p>
                    </div>
</a>
                    <hr>

                    <div class="resize-triggers">
                        <div class="expand-trigger">

                            <div class="contract-trigger"></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

    </div>


    <div class="row">
						<div class="col-12 col-lg-8">

							<div class="card radius-15">

								<div class="card-body">
                                <div class="card-title">
                        <h5 class="mb-0">Members Packages</h5>
                    </div>
                    <hr>
									<div id="chart11"></div>
								</div>
							</div>
						</div>
    <!--end row-->





    <div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Balance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                            aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th> ID</th>
                                <th> Name</th>
                                <th> Timing</th>
                                <th> Phone</th>
                                <th>Balance</th>
                                <th>Actions</th>
                                <!-- <th>Actions</th> -->
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($dashboard[0]['balance_details'] as $balance)
                            <tr>
                                <td>{{$balance->id}}</td>
                                <td>{{$balance->member_name}}</td>
                                <td>{{$balance->member_timings}}</td>
                                <td>{{$balance->member_phone}}</td>
                                <td>{{number_format($balance->balance)}}</td>
                                <td class="text-center">
                                    <a href="{{route('admin.member.view',$balance->id)}}" style="padding: 4px;"
                                        class="btn btn-success"><i class="lni lni-eye"></i>
                                </td>
                                <!-- <td>eye</td> -->
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th> ID</th>
                                <th> Name</th>
                                <th> Timing</th>
                                <th> Phone</th>
                                <th>Balance</th>
                                <th>Actions</th>
                                <!-- <th>Actions</th> -->
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Active Member</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['a_member'] as $active)
                        <tr>
                            <td>{{$active->id}}</td>
                            <td>{{$active->member_name}}</td>
                            <td>{{$active->member_timings}}</td>
                            <td>{{$active->member_phone}}</td>
                            <td>{{number_format($active->balance)}}</td>
                            <td>{{number_format($active->total)}}</td>
                            <td>
                            @if($active->member_image != '')
                            <img src="{{asset('storage/members/'.$active->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>

                            <td class="text-center">
                                <a href="{{route('admin.member.view',$active->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>
                            <!-- <td>eye</td> -->
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="exampleModal8" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Freeze Member</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['f_member'] as $freeze)
                        <tr>
                            <td>{{$freeze->id}}</td>
                            <td>{{$freeze->member_name}}</td>
                            <td>{{$freeze->member_timings}}</td>
                            <td>{{$freeze->member_phone}}</td>
                            <td>{{number_format($freeze->balance)}}</td>
                            <td>{{number_format($freeze->total)}}</td>
                            <td>
                            @if($freeze->member_image != '')
                            <img src="{{asset('storage/members/'.$freeze->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$freeze->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="exampleModal9" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Trainers</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                            <th> Name</th>
                            <th> Fee </th>
                            <th> Gender</th>
                            <th>Image</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['all_trainer'] as $trainer)
                        <tr>

                            <td>{{$trainer->trainer_name}}</td>
                            <td>{{number_format($trainer->trainer_fee)}}</td>
                            <td>{{$trainer->gender}}</td>
                            <td>
                                @if($trainer->trainer_image != '')
                                <img src="{{asset('storage/trainers/'.$trainer->trainer_image)}}" alt="" height="50px"
                                    width="50px" style="border-radius: 6px;">
                                @else
                                <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px"
                                    width="50px" style="border-radius: 6px;">
                                @endif

                            </td>


                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th> Name</th>
                            <th> Fee </th>
                            <th> Gender</th>
                            <th>Image</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal10" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Today Collection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['today_amount_details'] as $today_amount_detail)
                        <tr>

                        <td>{{$today_amount_detail->id}}</td>
                            <td>{{$today_amount_detail->member_name}}</td>
                            <td>{{$today_amount_detail->member_timings}}</td>
                            <td>{{$today_amount_detail->member_phone}}</td>
                            <td>{{number_format($today_amount_detail->balance)}}</td>
                            <td>{{number_format($today_amount_detail->total)}}</td>
                            <td>
                            @if($today_amount_detail->member_image != '')
                            <img src="{{asset('storage/members/'.$today_amount_detail->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$today_amount_detail->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal11" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">15 Days Collection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['fiftyday_amount_details'] as $fiftyday_amount_detail)
                        <tr>

                        <td>{{$fiftyday_amount_detail->id}}</td>
                            <td>{{$fiftyday_amount_detail->member_name}}</td>
                            <td>{{$fiftyday_amount_detail->member_timings}}</td>
                            <td>{{$fiftyday_amount_detail->member_phone}}</td>
                            <td>{{number_format($fiftyday_amount_detail->balance)}}</td>
                            <td>{{number_format($fiftyday_amount_detail->total)}}</td>
                            <td>
                            @if($fiftyday_amount_detail->member_image != '')
                            <img src="{{asset('storage/members/'.$fiftyday_amount_detail->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$fiftyday_amount_detail->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal12" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Monthly Collection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['monthly_amount_details'] as $monthly_amount_detail)
                        <tr>

                        <td>{{$monthly_amount_detail->id}}</td>
                            <td>{{$monthly_amount_detail->member_name}}</td>
                            <td>{{$monthly_amount_detail->member_timings}}</td>
                            <td>{{$monthly_amount_detail->member_phone}}</td>
                            <td>{{number_format($monthly_amount_detail->balance)}}</td>
                            <td>{{number_format($monthly_amount_detail->total)}}</td>
                            <td>
                            @if($monthly_amount_detail->member_image != '')
                            <img src="{{asset('storage/members/'.$monthly_amount_detail->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$monthly_amount_detail->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal13" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Today Expire</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['today_expire_details'] as $today_expire_detail)
                        <tr>

                        <td>{{$today_expire_detail->id}}</td>
                            <td>{{$today_expire_detail->member_name}}</td>
                            <td>{{$today_expire_detail->member_timings}}</td>
                            <td>{{$today_expire_detail->member_phone}}</td>
                            <td>{{number_format($today_expire_detail->balance)}}</td>
                            <td>{{number_format($today_expire_detail->total)}}</td>
                            <td>
                            @if($today_expire_detail->member_image != '')
                            <img src="{{asset('storage/members/'.$today_expire_detail->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$today_expire_detail->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal14" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">15 Days Expire</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['fiftyday_expire_details'] as $fiftyday_expire_detail)
                        <tr>

                        <td>{{$fiftyday_expire_detail->id}}</td>
                            <td>{{$fiftyday_expire_detail->member_name}}</td>
                            <td>{{$fiftyday_expire_detail->member_timings}}</td>
                            <td>{{$fiftyday_expire_detail->member_phone}}</td>
                            <td>{{number_format($fiftyday_expire_detail->balance)}}</td>
                            <td>{{number_format($fiftyday_expire_detail->total)}}</td>
                            <td>
                            @if($fiftyday_expire_detail->member_image != '')
                            <img src="{{asset('storage/members/'.$fiftyday_expire_detail->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$fiftyday_expire_detail->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal15" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Monthly Expire</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($dashboard[0]['monthly_expire_details'] as $monthly_expire_detail)
                        <tr>

                        <td>{{$monthly_expire_detail->id}}</td>
                            <td>{{$monthly_expire_detail->member_name}}</td>
                            <td>{{$monthly_expire_detail->member_timings}}</td>
                            <td>{{$monthly_expire_detail->member_phone}}</td>
                            <td>{{number_format($monthly_expire_detail->balance)}}</td>
                            <td>{{number_format($monthly_expire_detail->total)}}</td>
                            <td>
                            @if($monthly_expire_detail->member_image != '')
                            <img src="{{asset('storage/members/'.$monthly_expire_detail->member_image)}}" alt="" height="50px" width="50px" style="border-radius: 6px;">
                            @else
                            <img src="{{asset('fitnessback/assets/images/icons/altimg.jpg')}}" alt="" height="50px" width="50px"  style="border-radius: 6px;">
                            @endif
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.member.view',$fiftyday_expire_detail->id)}}" style="padding: 4px;"
                                    class="btn btn-success"><i class="lni lni-eye"></i>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th> ID</th>
                            <th> Name</th>
                            <th> Timing</th>
                            <th> Phone</th>
                            <th>Balance</th>
                            <th>Total</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

</div>
</div>




@include('admin.Partials.scripts')
<script src="{{asset('fitnessback/assets/plugins/apexcharts-bundle/js/apexcharts.min.js')}}"></script>
	<script src="{{asset('fitnessback/assets/plugins/apexcharts-bundle/js/apex-custom.js')}}"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();

});
$( function() {
    $( "#exampleModal6" ).draggable();
    $( "#exampleModal7" ).draggable();
    $( "#exampleModal8" ).draggable();
    $( "#exampleModal9" ).draggable();
    $( "#exampleModal10" ).draggable();
    $( "#exampleModal11" ).draggable();
    $( "#exampleModal12" ).draggable();
    $( "#exampleModal13" ).draggable();
    $( "#exampleModal14" ).draggable();
    $( "#exampleModal15" ).draggable();
    $( "#exampleModal16" ).draggable();
    $( "#exampleModal17" ).draggable();
  } );

</script>


@endsection
